const mapObject = require('../mapObject');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const result = mapObject(testObject, function(num){
    if(typeof(num) === "number"){
        return num * num;
    }else{
        return num;
    }
});
console.log(result);