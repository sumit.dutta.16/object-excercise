module.exports = function(obj, callback){
    for(let key in obj){
        obj[key] = callback(obj[key]);
    }
    return obj;
}
