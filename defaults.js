module.exports = function (obj, defaultprops){
    for(let key in obj){
        if(obj[key] === undefined){
            obj[key] = defaultprops[key];
        }
    }
    return obj;
}